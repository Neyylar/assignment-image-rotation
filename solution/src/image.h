#include <stdint.h>

#ifndef __IMAGE_H__
#define __IMAGE_H__

struct pixel{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

enum creation_status{
  CREATION_OK = 1,
  CREATION_FAILURE
};

enum creation_status create_image(uint64_t width, uint64_t height, struct image *img);
void free_image(struct image *img);

enum creation_status create_rotated(struct image const source, struct image *img);


#endif
