#include "image.h"
#include <stdlib.h>

enum creation_status create_image(uint64_t width, uint64_t height, struct image *img){
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if(!img->data)
        return CREATION_FAILURE;
    return CREATION_OK;
}

void free_image(struct image *img){
    free(img->data);
}

enum creation_status create_rotated(struct image const source, struct image *img){
    if(!create_image(source.height, source.width, img))
        return CREATION_FAILURE;
    int row = 0;
    int col = 0;
    for(row=0; row<img->height; row++){
        for(col=0; col<img->width; col++){
            size_t src_row = img->width-col-1;
            size_t src_col = row;
            img->data[img->width*row +col] = source.data[source.width*src_row + src_col];
        }
    }

    return CREATION_OK;
}
