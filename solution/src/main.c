#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

static const char *READ_ERROR_MESSAGES [] = {
    [READ_INVALID_BITS_COUNT] = "Pixel format should be 24-bits\n",
    [READ_INVALID_BITS] = "Invalid file format: invalid bits\n",
    [READ_INVALID_HEADER] = "Invalid file format: invalid header\n",
    [READ_INVALID_SIGNATURE] = "Invalid signatire\n",
    [READ_CANNOT_CREATE_IMAGE] = "Cannot create image\n"

};

int main(int argc, char **argv){
    if(argc < 3){
        fprintf(stderr, "Usage: ./rotate input output\n");
        return 126;
    }

    FILE *in = fopen(argv[1], "r");
    if(!in){
        fprintf(stderr, "Cannot open file %s\n", argv[1]);
        return 126;
    }
    FILE *out = fopen(argv[2], "w");
    if(!out){
        fprintf(stderr, "Cannot open file %s\n", argv[2]);
        fclose(in);
        return 126;
    }
    struct image img;
    enum read_status res = from_bmp(in, &img);
    fclose(in);
    if(res != READ_OK){
        fprintf(stderr, "%s", READ_ERROR_MESSAGES[res]);
        free_image(&img);
        fclose(out);
        return 1;
    }

    struct image img2;
    if(create_rotated(img, &img2) != CREATION_OK){
        fprintf(stderr, "Cannot write result\n");
        fclose(out);
        return 1;
    }
    free_image(&img);    

    if(to_bmp(out, img2) != WRITE_OK){
        fprintf(stderr, "Cannot write result\n");
    }

    free_image(&img2);
    
    fclose(out);
    return 0;
}
