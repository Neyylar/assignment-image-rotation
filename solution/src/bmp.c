#include "bmp.h"
#include "image.h"
#include <stdlib.h>
#include <string.h>

#define BITS_COUNT 24
#define FORMAT 0x4D42
#define PIXEL_INFO_POSITION sizeof(struct bmp_header)
#define HEADER_SIZE 40

static uint32_t get_bmp_padding(uint32_t width){
    uint32_t padding = 0;
    if(width % 4){
        padding = 4 - (width % 4);
    }
    return padding;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;

    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_HEADER;
    }

    if(header.biBitCount != BITS_COUNT){
        return READ_INVALID_BITS_COUNT;
    }

    if(create_image(header.biWidth, header.biHeight, img) != CREATION_OK)
        return READ_CANNOT_CREATE_IMAGE;

    const size_t width_bytes = sizeof(struct pixel) * img->width;
    uint32_t padding = get_bmp_padding(width_bytes);

    uint8_t *padding_bytes = NULL;
    if(padding > 0){
        padding_bytes = malloc(padding);
        if(!padding_bytes){
            return READ_CANNOT_CREATE_IMAGE;
        }
    }
    int i=0;
    for(i=0; i<padding; i++){
        padding_bytes[i] = 0;
    }

    int row = 0;
    for(row=0; row<img->height; row++){
        struct pixel *pos = img->data + row * img->width;
        
        if(fread(pos, sizeof(struct pixel), img->width, in) != img->width){
            free(padding_bytes);
            return READ_INVALID_BITS;
        }


        if(padding > 0 && fread(padding_bytes, padding, 1, in) != 1){
            return READ_INVALID_BITS;
        }

    }

    free(padding_bytes);

    return READ_OK;
}

static struct bmp_header create_bmp_header(uint32_t width, uint32_t height){
    uint32_t padding = get_bmp_padding(width);
    struct bmp_header header;
    header.bfType = FORMAT;
    header.bfileSize = sizeof(struct pixel) * width * height + sizeof(struct bmp_header);
    header.bfReserved = 0;
    header.bOffBits = PIXEL_INFO_POSITION;
    header.biSize = HEADER_SIZE;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = BITS_COUNT;
    header.biCompression = 0;
    header.biSizeImage = (sizeof(struct pixel)*width + padding) * height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum write_status to_bmp(FILE *out, struct image img){
    const size_t width_bytes = sizeof(struct pixel) * img.width;
    size_t padding = get_bmp_padding(width_bytes);

    struct bmp_header header = create_bmp_header(img.width, img.height);

    if(fwrite(&header, sizeof(header), 1, out) != 1){
        return WRITE_FAIL;
    }

    uint8_t *padding_bytes = NULL;
    if(padding > 0){
        padding_bytes = malloc(padding);
        if(!padding_bytes){
            return WRITE_FAIL;
        }
    }
    int i=0;
    for(i=0; i<padding; i++){
        padding_bytes[i] = 0;
    }

    int row;
    for(row=0; row<img.height; row++){
        
        if(fwrite(img.data+img.width*row, sizeof(struct pixel), img.width, out) != img.width){
            free(padding_bytes);
            return WRITE_FAIL;
        }
        if(padding > 0 && fwrite(padding_bytes, padding, 1, out) != 1){
            free(padding_bytes);
            return WRITE_FAIL;
        }

    }
    free(padding_bytes);
    return WRITE_OK;
}
